FROM python:latest

ENV SRC_PATH /app/src

COPY . $SRC_PATH

WORKDIR $SRC_PATH

RUN python setup.py install

CMD ["python", "/app/src/src/tensorflow_python_test/app.py" ]