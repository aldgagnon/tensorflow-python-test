import tensorflow as tf

IMG_SIZE = (160, 160)


# build model

def create_base_model(train_dataset):
  ## instantiate a MobileNet V2 model pre-loaded with weights trained on ImageNet
  # Create the base model from the pre-trained model MobileNet V2
  # include_top = false means we scrape off the classification layer
  IMG_SHAPE = IMG_SIZE + (3,)
  base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                                 include_top=False,
                                                 weights='imagenet')

  image_batch, label_batch = next(iter(train_dataset))
  feature_batch = base_model(image_batch)

  ## freeze the convolutional base

  base_model.trainable = False

  ## add classification head

  global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
  feature_batch_average = global_average_layer(feature_batch)
  print(feature_batch_average.shape)

  ## convert features into a single prediction per image

  prediction_layer = tf.keras.layers.Dense(1)
  prediction_batch = prediction_layer(feature_batch_average)
  print(prediction_batch.shape)

  return base_model, prediction_layer, global_average_layer 


def build_model(base_model, prediction_layer, global_average_layer, data_augmentation, preprocess_input):
  inputs = tf.keras.Input(shape=(160, 160, 3))
  x = data_augmentation(inputs)
  x = preprocess_input(x)
  x = base_model(x, training=False)
  x = global_average_layer(x)
  x = tf.keras.layers.Dropout(0.2)(x)
  outputs = prediction_layer(x)
  model = tf.keras.Model(inputs, outputs)

  return model


def compile_model(model):
    base_learning_rate = 0.0001

    model.compile(optimizer=tf.keras.optimizers.Adam(lr=base_learning_rate),
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])

def train_model(initial_epochs, model, validation_dataset, train_dataset):
    #checkpoint_path = "training_1/cp.ckpt"
    #checkpoint_dir = os.path.dirname(checkpoint_path)

    # add checkpoints
    # Create a callback that saves the model's weights
    #cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
    #                                                 save_weights_only=True,
    #                                                 verbose=1)
    
    loss0, accuracy0 = model.evaluate(validation_dataset)

    history = model.fit(train_dataset,
                        epochs=initial_epochs,
                        validation_data=validation_dataset)
                        #,callbacks=[cp_callback])